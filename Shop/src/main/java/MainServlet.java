import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/main")
public class MainServlet extends HttpServlet {
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
            if (session.getAttribute("value").equals("false")) response.sendRedirect("auth");
            else {
                request.setAttribute("name", session.getAttribute("Login"));
                request.setAttribute("link", "\"https://farm8.staticflickr.com/7326/11287113923_57d37ed9d3_q.jpg\"");
                request.getRequestDispatcher("main.jsp").forward(request, response);
            }
    }
}
