import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/auth")
public class AuthServlet extends HttpServlet {
    String login;
    String pass;

    public void init(ServletConfig config) throws ServletException {
        ServletContext sc = config.getServletContext();
        login = sc.getInitParameter("login");
        pass = sc.getInitParameter("pass");
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("auth.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //if (request.getParameter("login").equals("admin") && request.getParameter("pass").equals("admin")) {
        if (login.equals("admin") && pass.equals("admin")) {
            ServletContext context = request.getSession().getServletContext();
            context.setAttribute("someValue", "aValue");
            HttpSession session = request.getSession();
            session.setAttribute("Auth", context);
            session.setAttribute("Login", login);
            request.setAttribute("session", session);
            response.sendRedirect("main");
        }
    }
}
