import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

public class MainFilter implements Filter {
    public void init(FilterConfig config) throws ServletException {
        System.out.println("Filter is working now ");
    }

    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String filter;
        HttpSession session = request.getSession();
        System.out.println(session.getId());
        ServletContext context = (ServletContext) session.getAttribute("Auth");
        try {
            if (context.getAttribute("someValue").equals("aValue")) {
                filter = "true";
            } else {
                filter = "false";
            }
        } catch (NullPointerException e) {
            filter = "false";
        }
        session.setAttribute("value", filter);
        req.setAttribute("session", session);
        chain.doFilter(request, response);
    }

    public void destroy() {

    }
}
