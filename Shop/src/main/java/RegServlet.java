import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/reg")
public class RegServlet extends HttpServlet {
    String login;
    String pass;
    String name;
    String surename;

    public void init(ServletConfig config) throws ServletException {
        ServletContext sc = config.getServletContext();
        login = sc.getInitParameter("login");
        pass = sc.getInitParameter("pass");
        name = sc.getInitParameter("name");
        surename = sc.getInitParameter("surename");
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("reg.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            ServletContext context = request.getSession().getServletContext();
            context.setAttribute("someValue", "aValue");
            HttpSession session = request.getSession();
            session.setAttribute("Auth", context);
            session.setAttribute("Login", login);
            request.setAttribute("session", session);
            response.sendRedirect("main");
    }
}
