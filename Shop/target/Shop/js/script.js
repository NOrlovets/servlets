$(document).ready(function () {
    $(".btn-info").click(function () {
        $(".search").fadeIn(300);
    });

    $(".search > span").click(function () {
        $(".search").fadeOut(300);
    });
});

$(document).ready(function () {
    $(".btn-primary").click(function () {
        $(".shop").fadeIn(300);
        $("#blur").addClass('blur-in');
    });

    $(".shop > span").click(function () {
        $("#blur").removeClass('blur-in');
        $(".shop").fadeOut(300);
    });
});

$(document).ready(function () {
    $(".button").click(function () {
        $(".product").fadeIn(300);
        var elemY = (this).offsetTop;
        $(".product").css('top', elemY);
        $(".description").fadeIn(300);
    });

    $(".product > span").click(function () {
        $(".product").fadeOut(300);
    });
});

$(document).ready(function () {
    var goods = 0;
    $("#butcen").click(function () {
        goods++;
        $('#cart').html(goods);
    });
});

$('#password, #checkpassword').on('keyup', function () {
    if ($('#password').val() == $('#checkpassword').val()) {
        $('#message').html('Matching').css('color', 'green');
    } else
        $('#message').html('Not Matching').css('color', 'red');
});
