<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/themes.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="js/script.js"></script>


    <title>University of Epam</title>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li><a href="#"><span class="glyphicon glyphicon-user"> ${name}</span></a></li>
            <li><a href="/logout"><span class="glyphicon glyphicon-log-out"> Exit</span></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <button type="button" class="btn btn-info reg"> Search..</button>
            </li>
            <p class="navbar-text" id="cart">0</p>
            <li>
                <button type="button" class="btn btn-primary reg"> Shop</button>
            </li>
        </ul>
    </div>
</nav>
<br><br>


<div id="blur">


    <h1>Catalog</h1>


    <dl class="authors">
        <dt>A</dt>
        <c:forEach var="i" begin="1" end="15">
            <div class="box button">
                <img src=${link}/>
                <p>Title</p>
            </div>
        </c:forEach>


        <dt>B</dt>
        <c:forEach var="i" begin="1" end="15">
            <div class="box button">
                <img src=${link}/>
                <p>Title</p>
            </div>
        </c:forEach>
    </dl>

</div>
<div class="product description">
    <span>✖</span>

    <img src="https://farm8.staticflickr.com/7326/11287113923_57d37ed9d3_q.jpg" id="img"/>

    <div class="price">564$</div>
    <button type="button" name="button" id="butcen">Buy</button>

</div>

<div class="search">
    <span>✖</span>

    <h1>search</h1>

    <p>This is a random pop up, hopefully its not annoying.</p>
</div>

<div class="shop">
    <span>✖</span>

    <h1>shop</h1>

    <p>This is a random pop up, hopefully its not annoying.</p>
</div>

<div class="footer">This footer will always be positioned at the bottom of the page</div>


</body>
</html>

