<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <link rel="stylesheet" href="css/themes.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <title>Reg</title>
</head>

<body>

<div class="login-page">
    <div class="form">
        <h1 class="upmessage">Registration</h1>

        <form action="reg" method="POST">
            <input type="text" name="name" placeholder="name"><br>
            <input type="text" name="surename" placeholder="surename"><br>
            <input type="text" name="login" placeholder="login"><br>
            <input type="password" name="password" placeholder="password" id="password"><br>
            <input type="password" name="checkpassword" placeholder="checkpassword" id="checkpassword"> <span id='message'></span><br>
            <button>create</button>
        </form>
        <p class="message"><a href="auth">Authorization</a></p>
    </div>
</div>
<script src="js/script.js"></script>
</body>

</html>
